import React, { useState } from "react";
import jsonData from "./../JsonFiles/test1.json";
import * as Styled from "./styled";

const Test1 = () => {
  const [state, setState] = useState({
    data: {
      columns: jsonData.columns,
      originalRows: jsonData.rows,
      rows: jsonData.rows
    },
    search: "",
    type: "",
    question: "",
    error: false,
    successfull: false
  });

  const handleAdd = () => {
    const { type, question, data } = state;
    if (type === "" || question === "") return;
    let order = data.rows.length;
    setState({
      ...state,
      type: "",
      question: "",
      data: {
        ...state.data,
        rows: [
          ...state.data.rows,
          {
            type,
            question,
            order: order + 1
          }
        ],
        originalRows: [
          ...state.data.originalRows,
          {
            type,
            question,
            order: order + 1
          }
        ]
      }
    });
  };
  const handleSearch = () => {
    const { search, data } = state;
    const filteredData = search
      ? data.rows.filter(
        x =>
          x.question.indexOf(search) !== -1 || x.type.indexOf(search) !== -1
      )
      : data.originalRows;

    setState({
      ...state,
      search: "",
      data: {
        ...state.data,
        rows: filteredData
      }
    });
  };

  return (
    <Styled.Wraper>
      <Styled.FormWrap>
        <Styled.FormItem>
          <Styled.Input
            value={state.type}
            placeholder="Enter Type..."
            onChange={e => setState({ ...state, type: e.target.value })}
          />
          <Styled.Input
            value={state.question}
            placeholder="Enter Question..."
            onChange={e => setState({ ...state, question: e.target.value })}
          />
          <Styled.Button onClick={handleAdd}>Add</Styled.Button>
        </Styled.FormItem>
        <Styled.FormItem>
          <Styled.Input
            width="430px"
            value={state.search}
            placeholder="Enter keyword..."
            onChange={e => setState({ ...state, search: e.target.value })}
          />
          <Styled.Button onClick={handleSearch}>Search</Styled.Button>
        </Styled.FormItem>
        <Styled.FormItem>
          <Styled.Table>
            <thead>
              <tr>
                {state.data.columns &&
                  state.data.columns.map((col, index) => {
                    return <Styled.Th key={index}>{col.label}</Styled.Th>;
                  })}
              </tr>
            </thead>
            <tbody>
              {state.data.rows &&
                state.data.rows
                  .sort((a, b) => b.order - a.order)
                  .map((row, index) => {
                    return (
                      <tr key={index}>
                        <Styled.Td>{row.order}</Styled.Td>
                        <Styled.Td>{row.type}</Styled.Td>
                        <Styled.Td>{row.question}</Styled.Td>
                      </tr>
                    );
                  })}
            </tbody>
          </Styled.Table>
        </Styled.FormItem>
      </Styled.FormWrap>
    </Styled.Wraper>
  );
};

export default Test1;
