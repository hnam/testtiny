import styled from "styled-components";

export const Wraper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  max-width: 50%;
  margin: 50px auto;
`;
export const FormWrap = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;
export const FormItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const Input = styled.input.attrs({
  type: "text",

  placeholder: props => props.placeholder || "Please fill"
})`
  width: ${props => props.width || "200px"};
  border: 1px solid #ddd;
  border-radius: 5px;
  padding: 8px;
  outline: none;
  margin: 5px;
`;

export const Button = styled.button`
  border-radius: 5px;
  display: inline-block;
  padding: 0.5rem 0;
  margin: 0.5rem 1rem;
  width: ${props => props.width || "5rem"};
  background: #66b9f4;
  color: white;
  cursor: pointer;
  text-align: center;
  color: #fff;
  border: none;
  &:hover {
    opacity: 0.8;
  }
`;

export const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
  background-color: #fff;
  color: #5e6d82;
  font-size: 14px;
  margin-bottom: 45px;
  line-height: 1.5em;
  margin-top: 20px;
`;

export const Th = styled.th`
  border-bottom: 1px solid #eaeefb;
  padding: 10px;
  max-width: 250px;
  text-align: left;
  border-top: 1px solid #eaeefb;
  background-color: #eff2f7;
  white-space: nowrap;
`;

export const Td = styled.td`
  border-bottom: 1px solid #eaeefb;
  padding: 10px;
  max-width: 250px;
  text-align: left;
`;
