import React, { useState, useEffect, useForceUpdate } from "react";
import { Input, DatePicker, Form, InputNumber, Select } from "element-react";
import "element-theme-default";
import * as Styled from "./styled";

const Test3 = () => {
  const [state, setState] = useState({
    modifier: [],
    owner: [],
    data: {},
    form: {
      fields: []
    }
  });

  const fetchApi = url => {
    return fetch(url)
      .then(response => response.json())
      .catch(error => console.error(error));
  };

  useEffect(() => {
    const loadData = async () => {
      const [modifier, owner, data, fields] = await Promise.all([
        fetchApi("http://localhost:3030/modifier"),
        fetchApi("http://localhost:3030/owner"),
        fetchApi("http://localhost:3030/data"),
        fetchApi("http://localhost:3030/structure")
      ]);
      let array = [];
      fields.map(item => {
        let obj = {};
        if (item.value === "modifier") {
          obj = Object.assign(item, {
            name: item.value,
            options: [...modifier.items, data[item.value]],
            selectedValue: `${data[item.value].value}`
          });
        } else if (item.value === "owner") {
          obj = Object.assign(item, {
            name: item.value,
            options: [...owner.items, data[item.value]],
            selectedValue: `${data[item.value].value}`
          });
        } else {
          obj = Object.assign(item, {
            value: data[item.value],
            name: item.value
          });
        }
        array.push(obj);
      });
      setState({
        modifier,
        owner,
        data,
        form: {
          fields: array
        }
      });
    };
    loadData();
  }, []);

  const { modifier, owner, data, form } = state;

  const onHandleChange = (index, value) => {
    const { fields } = state.form;
    fields[index].value = value;
    setState({
      ...state,
      form: {
        fields
      }
    });
  };
  const InputRender = (item, index) => {
    return (
      <Form.Item
        label={item.label}
        prop={`fields:${index}`}
        rules={{
          type: "object",
          required: item.required,
          fields: {
            value: {
              required: item.required,
              message: `${item.label} can not be null`,
              trigger: "blur"
            }
          }
        }}
      >
        {item.valueType.value === "LONG" && (
          <InputNumber
            defaultValue={parseInt(item.value)}
            disabled={item.disabled}
            onChange={onHandleChange.bind(this, index)}
          />
        )}
        {item.valueType.value === "STRING" && (
          <Input
            value={item.value}
            disabled={item.disabled}
            onChange={onHandleChange.bind(this, index)}
          />
        )}
        {item.valueType.value === "REFERENCE" && (
          <Select value={item.selectedValue}>
            {item.options &&
              item.options.map((el, ind) => {
                return (
                  <Select.Option key={ind} label={el.label} value={el.value} />
                );
              })}
          </Select>
        )}
      </Form.Item>
    );
  };

  const DateRender = (item, index) => {
    return (
      <Form.Item
        key={index}
        label={item.label}
        required={item.required}
      >
        <DatePicker
          align="left"
          value={new Date(item.value * 1000)}
          placeholder="Pick a day"
          onChange={date => {
            onHandleChange(index, date.getTime() / 1000);
          }}
        />
      </Form.Item>
    )
  }

  return (
    <Styled.Wraper>
      <Form
        useRef="form"
        model={form}
        labelWidth="200"
        className="demo-dynamic"
      >
        {form.fields &&
          form.fields.map((item, index) => {
            return (
              <div key={index}>
                {(item.valueType.value === "STRING" ||
                  item.valueType.value === "LONG" ||
                  item.valueType.value === "REFERENCE") &&
                  InputRender(item, index, data)}
                {item.valueType.value === "DATE" && DateRender(item, index)}
              </div>
            );
          })}
      </Form>
    </Styled.Wraper>
  );
};

export default Test3;
