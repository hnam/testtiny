import React, { useState, useEffect } from "react";
import { Input, DatePicker, ColorPicker } from "element-react";
import "element-theme-default";
import * as Styled from "./styled";

const Test2 = () => {
  const [state, setState] = useState({
    dateData: "",
    inputData: "",
    color: ""
  });
  useEffect(() => {
    fetch(`http://www.mocky.io/v2/5d6509383400004e00f444fa`)
      .then(response => response.json())
      .then(data => {
        setState({
          ...data
        });
      })
      .catch(error => console.error(error));
  }, []);

  const { dateData, inputData, color } = state;

  return (
    <Styled.Wraper>
      <Styled.FormWrap>
        <Input
          value={inputData}
          onChange={value =>
            setState({
              ...state,
              inputData: value
            })
          }
        />
      </Styled.FormWrap>
      <Styled.FormWrap>
        <DatePicker
          value={new Date(dateData * 1000)}
          placeholder="Pick a day"
          onChange={date => {
            setState({
              ...state,
              dateData: date.getTime() / 1000
            });
          }}
        />
      </Styled.FormWrap>
      <Styled.FormWrap>
        <ColorPicker
          value={color}
          onChange={value => {
            setState({
              ...state,
              color: value
            });
          }}
        />
      </Styled.FormWrap>
    </Styled.Wraper>
  );
};

export default Test2;
