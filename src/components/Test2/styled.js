import styled from "styled-components";

export const Wraper = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  max-width: 50%;
  margin: 50px auto;
`;
export const FormWrap = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: flex-start;
  margin: 10px 0;
`;
