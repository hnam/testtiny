import React, { useState, useEffect } from "react";
import dataInput from "../JsonFiles/test4.json";
import { LineChart } from "react-chartkick";
import "chart.js";
import * as Styled from "./styled";

const Test4 = () => {

  Object.entries(dataInput).forEach(function f([k, v]) {
    v.data.Average = Math.ceil(
      (v.data.Teamwork + v.data.Benefit + v.data.Career) / 3
    );
    v.data.Total = v.data.Teamwork + v.data.Benefit + v.data.Career;
  });
  const [state, setState] = useState({ data: dataInput });

  return (
    <Styled.Wraper>
      <LineChart data={state.data} />
    </Styled.Wraper>
  );
};

export default Test4;






// import React, { Component } from "react";
// import data from "../JsonFiles/test4.json";
// import { LineChart } from "react-chartkick";
// import "chart.js";
// export default class Test4 extends Component {
//   constructor(props) {
//     super(props);
// const dataNew = Object.entries(data).forEach(function f([k, v]) {
//   v.data.Average = Math.ceil(
//     (v.data.Teamwork + v.data.Benefit + v.data.Career) / 3
//   );
//   v.data.Total = v.data.Teamwork + v.data.Benefit + v.data.Career;
// });
//   }

//   total() {
//     return;
//   }

//   render() {
//     return (
//       <div
//         style={{
//           width: "50%",
//           margin: "0 auto",
//           padding: "30px 0",
//           textAlign: "center"
//         }}
//       >
//         <h4>Render a datatable using test4.json</h4>
//         <p>Note: Calculate Average value, Total value </p>
//         <LineChart data={data} />
//       </div>
//     );
//   }
// }
