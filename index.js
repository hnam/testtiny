import React from "react";
import ReactDOM from "react-dom";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";

import { i18n } from 'element-react'
import locale from 'element-react/src/locale/lang/en'
i18n.use(locale);

import Loadable from "./src/components/loadable";
const AsyncTest1 = Loadable(() => {
  return import('./src/components/Test1/Test1');
});
const AsyncTest2 = Loadable(() => {
  return import('./src/components/Test2/Test2');
});
const AsyncTest3 = Loadable(() => {
  return import('./src/components/Test3/Test3');
});
const AsyncTest4 = Loadable(() => {
  return import('./src/components/Test4/Test4');
});

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          {/* <Link to="/">Home</Link>
          <div className="text-center">
            <h1>I-ON Communications Code Test</h1>
            <p>
              This challenge contains 3 tests. Complete the tests and share us
              your GIT repository.
            </p>
            <h4>Test Pages Links</h4>
            <ul className="test-list">
              <li>
                <Link to="/test1">Test - 1</Link>
              </li>
              <li>
                <Link to="/test2">Test - 2</Link>
              </li>
              <li>
                <Link to="/test3">Test - 3</Link>
              </li>
              <li>
                <Link to="/test4">Test - 4</Link>
              </li>
            </ul>
          </div> */}

          <Route path="/test1" component={AsyncTest1} />
          <Route path="/test2" component={AsyncTest2} />
          <Route path="/test3" component={AsyncTest3} />
          <Route path="/test4" component={AsyncTest4} />
        </div>
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));
